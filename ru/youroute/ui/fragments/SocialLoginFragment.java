package ru.youroute.ui.fragments;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.SimpleType;
import org.apache.http.HttpStatus;
import java.util.HashMap;
import ru.youroute.R;
import ru.youroute.YourouteApplication;
import ru.youroute.model.User;
import ru.youroute.ui.BaseActivity;
import ru.youroute.ui.Fragment_Base;
import ru.youroute.ui.utils.socialauth.BaseSocialAuth;
import ru.youroute.ui.utils.socialauth.FacebookAuth;
import ru.youroute.ui.utils.socialauth.SocialAuthState;
import ru.youroute.ui.utils.socialauth.TwitterAuth;
import ru.youroute.ui.utils.socialauth.VkontakteAuth;
import ru.youroute.utils.TextHelper;
import ru.youroute.utils.Utils;
import ru.youroute.utils.YourouteNotificationManager;
import ru.youroute.utils.internet.BackgroundService;
import ru.youroute.utils.internet.YourouteRequest;
import ru.youroute.utils.internet.jackson.JacksonRequestListener;

/**
 * Created by gang018 on 05.11.14.
 */
public class SocialLoginFragment extends Fragment_Base
{
    private static final String KEY_PROVIDER = "provider",
                                KEY_ACCESS_TOKEN = "access_token";

    private BaseSocialAuth mAuthProvider;

    private BaseSocialAuth.SocialLoginState mCallback = new BaseSocialAuth.SocialLoginState()
    {
        @Override
        public void onLoginComplete()
        {
            final SocialAuthState state = mAuthProvider.getState();
            if (!TextHelper.isEmpty(state.getState()))
            {
                if (state.getState().equals(SocialAuthState.STATE_SUCCESS))
                {
                    loginSocial();
                }
                else if (state.equals(SocialAuthState.STATE_FAILED))
                {
                    Toast.makeText(getActivity(), R.string.error_social_failed, Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(getActivity(), R.string.error_social_failed, Toast.LENGTH_LONG).show();
            }
        }
    };

    private void loginSocial()
    {
        if (getActivity() != null)
        {
            final ProgressDialog dialog = Utils.showProgressDialog(getActivity());

            final SocialAuthState state = mAuthProvider.getState();
            final HashMap<String, String> params = new HashMap<String, String>();
            params.put(KEY_PROVIDER, state.getProvider());
            params.put(KEY_ACCESS_TOKEN, state.getToken());

            final YourouteRequest<User> request = new YourouteRequest<User>(Request.Method.POST, BackgroundService.API_AUTH_LOGIN, params,
                    new JacksonRequestListener<User>()
                    {
                        @Override
                        public void onResponse(final User response, int statusCode, VolleyError error)
                        {
                            Utils.closeDialog(getActivity(), dialog);

                            if (statusCode == HttpStatus.SC_OK)
                            {
                                if (response != null)
                                {
                                    if (!TextHelper.isEmpty(response.user_id))
                                    {
                                        final YourouteApplication app = YourouteApplication.getInstance();
                                        app.setUser(response);

                                        if (getActivity() != null && !getActivity().isFinishing())
                                        {
                                            getActivity().setResult(Activity.RESULT_OK);
                                            getActivity().finish();
                                        }
                                    }
                                    else
                                    {
                                        Utils.showError(getActivity(), response.message);
                                    }
                                }
                                else
                                {
                                    Utils.showError((BaseActivity) getActivity(), getString(R.string.error_internet_connection));
                                }
                            }
                            else
                            {
                                Utils.showError((BaseActivity) getActivity(), getString(R.string.error_internet_connection));
                            }
                        }

                        @Override
                        public JavaType getReturnType()
                        {
                            return SimpleType.construct(User.class);
                        }
                    });

            YourouteApplication.getInstance().addToRequestQueue(request);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        mParent = inflater.inflate(R.layout.fragment_social_login, null, true);

        handleRecreation(savedInstanceState);

        View btn = mParent.findViewById(R.id.fragment_login_btn_login_simple);
        btn.setOnClickListener(this);
        btn = mParent.findViewById(R.id.fragment_social_login_fb);
        btn.setOnClickListener(this);
        btn = mParent.findViewById(R.id.fragment_social_login_vk);
        btn.setOnClickListener(this);
        btn = mParent.findViewById(R.id.fragment_social_login_mail);
        btn.setOnClickListener(this);
        btn = mParent.findViewById(R.id.fragment_social_login_gplus);
        btn.setOnClickListener(this);
        btn = mParent.findViewById(R.id.fragment_social_login_od);
        btn.setOnClickListener(this);
        btn = mParent.findViewById(R.id.fragment_social_login_tw);
        btn.setOnClickListener(this);

        return mParent;
    }

    private void handleRecreation(final Bundle state)
    {
        try
        {
            if (state != null)
            {
                final Class<?> cl = Class.forName(state.getString(BaseSocialAuth.class.getName()));
                mAuthProvider = (BaseSocialAuth) cl.getDeclaredConstructor(Bundle.class).newInstance(state);
                mAuthProvider.setActivity(getActivity());
                mAuthProvider.setCallback(mCallback);
            }
        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.fragment_login_btn_login_simple:
            {
                YourouteNotificationManager.sendNotification(getActivity(), YourouteNotificationManager.NOTIFICATION_AUTH_REGISTRATION);
                break;
            }

            case R.id.fragment_social_login_vk:
            {
                mAuthProvider = new VkontakteAuth(getActivity(), mCallback);
                mAuthProvider.startAuthorization();
                break;
            }

            case R.id.fragment_social_login_tw:
            {
                mAuthProvider = new TwitterAuth(getActivity(), mCallback);
                mAuthProvider.startAuthorization();
                break;
            }

            case R.id.fragment_social_login_fb:
            {
                mAuthProvider = new FacebookAuth(getActivity(), mCallback);
                mAuthProvider.startAuthorization();
                break;
            }
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mAuthProvider != null)
            mAuthProvider.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (mAuthProvider != null)
            mAuthProvider.onResume();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (mAuthProvider != null)
            mAuthProvider.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (mAuthProvider != null)
            mAuthProvider.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle arg0)
    {
        if (mAuthProvider != null)
        {
            arg0.putString(BaseSocialAuth.class.getName(), mAuthProvider.getClass().getName());
            mAuthProvider.onSaveInstanceState(arg0);
        }

        super.onSaveInstanceState(arg0);
    }
}
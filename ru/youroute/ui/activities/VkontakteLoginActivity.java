/**
 * Author - gang018
 * Email - gang1812@gmail.com
 * 03.11.2014
 */
package ru.youroute.ui.activities;
import java.net.URLEncoder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import ru.youroute.R;
import ru.youroute.ui.BaseActivity;
import ru.youroute.ui.utils.socialauth.BaseSocialAuth;
import ru.youroute.utils.Logger;
import ru.youroute.utils.Utils;

public class VkontakteLoginActivity extends BaseActivity
{
	private static final String APP_ID = "",
								REDIRECT_URL = "http://api.vk.com/blank.html";

	private static final int PROGRESS_BAR_HEIGHT_MULT = 1/7,
								VK_ACCOUNT_PERMISSIONS = 8192;
	
	private WebView webview;
	private ProgressBar mProgress;
	
	@SuppressWarnings("deprecation")
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_social);
		
		initProgressBar();
		webview = (WebView) findViewById(R.id.social_webview);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.clearCache(true);
		webview.setWebChromeClient(new WebChromeClient()
        {
			@Override
			public void onProgressChanged(WebView view, int newProgress) 
			{
				super.onProgressChanged(view, newProgress);
				mProgress.setProgress(newProgress);
			}
        });
		
		webview.setWebViewClient(new VkontakteWebViewClient());
		
		//otherwise CookieManager will fall with java.lang.IllegalStateException: CookieSyncManager::createInstance() needs to be called before CookieSyncManager::getInstance()
		CookieSyncManager.createInstance(this);
		
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.removeAllCookie();
		
		webview.loadUrl(getVkLoginUrl());
	}
	
	private void initProgressBar()
    {
        mProgress = (ProgressBar) findViewById(R.id.social_progress);
        final int height = PROGRESS_BAR_HEIGHT_MULT * Utils.getStatusBarHeight(this);
        
        final LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, height);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        mProgress.setLayoutParams(params);
    }
	
	@SuppressWarnings("deprecation")
	private String getVkLoginUrl()
	{
		final String result = "http://oauth.vk.com/authorize?client_id=" + APP_ID +
				  "&display=touch&scope=" + Integer.toString(VK_ACCOUNT_PERMISSIONS) + // 65536
				  "&redirect_uri=" + URLEncoder.encode(REDIRECT_URL) + "&response_type=token";
		return result;
	}
	
	class VkontakteWebViewClient extends WebViewClient 
	{
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) 
		{
			super.onPageStarted(view, url, favicon);
			parseUrl(url);
		}
	}
	
	private void parseUrl(String url) 
	{
		try 
		{
			if(url==null)
				return;
			Logger.i("Redirect url is " + url);
			if(url.startsWith(REDIRECT_URL))
			{
				if(!url.contains("error="))
				{
					int start = url.indexOf('=');
					int end = url.indexOf('&', start);
					final String code;
					if (end == -1)
						code = url.substring(start + 1);//url.length());
					else
						code = url.substring(start + 1, end);
		
					final Intent intent = new Intent();
					intent.putExtra(BaseSocialAuth.KEY_TOKEN, code);
					setResult(Activity.RESULT_OK, intent);
				}
				else
					setResult(Activity.RESULT_CANCELED);
				finish();
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@Override
	protected int getPhoneOrientation()
	{
		return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
	}

	@Override
	protected int getTabletOrientation()
	{
		return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
	}
}
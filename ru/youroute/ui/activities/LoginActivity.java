package ru.youroute.ui.activities;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import ru.youroute.R;
import ru.youroute.ui.BaseActivity;
import ru.youroute.ui.utils.pageradapter.AuthPagerAdapter;
import ru.youroute.utils.YourouteNotificationManager;

/**
 * Created by gang018 on 05.11.14.
 */
public class LoginActivity extends BaseActivity
{
    private BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final String action = intent.getAction();
            if (action.equals(YourouteNotificationManager.NOTIFICATION_AUTH_REGISTRATION))
            {
                mPager.setCurrentItem(AuthPagerAdapter.POSITION_LOGIN_VIA_EMAIL);
            }
            else if (action.equals(YourouteNotificationManager.NOTIFICATION_AUTH_RESTORE_PASSWORD))
            {
                mPager.setCurrentItem(AuthPagerAdapter.POSITION_RESET_PASSWORD);
            }
        }
    };

    private ViewPager mPager;
    private AuthPagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle arg0)
    {
        super.onCreate(arg0);
        setContentView(R.layout.activity_login);

        mPager = (ViewPager) findViewById(R.id.activity_login_pager);
        mPager.setOffscreenPageLimit(3);
        mPager.setAdapter(mAdapter = new AuthPagerAdapter(getSupportFragmentManager()));

        registerReceiver();
    }

    private void registerReceiver()
    {
        final IntentFilter filter = new IntentFilter(YourouteNotificationManager.NOTIFICATION_AUTH_REGISTRATION);
        filter.addAction(YourouteNotificationManager.NOTIFICATION_AUTH_RESTORE_PASSWORD);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2)
    {
        super.onActivityResult(arg0, arg1, arg2);
        mAdapter.getItem(mPager.getCurrentItem()).onActivityResult(arg0, arg1, arg2);
    }

    @Override
    public void onBackPressed()
    {
        if (mPager.getCurrentItem() == AuthPagerAdapter.POSITION_LOGIN_VIA_EMAIL)
            mPager.setCurrentItem(AuthPagerAdapter.POSITION_LOGIN);
        else if (mPager.getCurrentItem() == AuthPagerAdapter.POSITION_RESET_PASSWORD)
            mPager.setCurrentItem(AuthPagerAdapter.POSITION_LOGIN_VIA_EMAIL);
        else
            super.onBackPressed();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected int getPhoneOrientation()
    {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }

    @Override
    protected int getTabletOrientation()
    {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }
}
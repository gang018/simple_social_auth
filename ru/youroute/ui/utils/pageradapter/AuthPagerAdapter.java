package ru.youroute.ui.utils.pageradapter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import ru.youroute.ui.fragments.LoginViaEmailFragment;
import ru.youroute.ui.fragments.PasswordRecoveryFragment;
import ru.youroute.ui.fragments.SocialLoginFragment;
import ru.youroute.utils.Constants;

public class AuthPagerAdapter extends FragmentStatePagerAdapter
{
	public static final int POSITION_LOGIN = Constants.ZERO,
								POSITION_LOGIN_VIA_EMAIL = POSITION_LOGIN + 1,
                                POSITION_RESET_PASSWORD = POSITION_LOGIN_VIA_EMAIL + 1,
								SIZE = 3;
	
	public AuthPagerAdapter(final FragmentManager fm)
	{
		super(fm);
	}
	
	@Override
	public int getCount()
	{
		return SIZE;
	}

	@Override
	public Fragment getItem(int position)
	{
		final Fragment fragment = getFragmentUsingReflection(position);
		if (fragment != null)
			return fragment;
		
		
		if (position == POSITION_LOGIN)
			return new SocialLoginFragment();
		else if (position == POSITION_LOGIN_VIA_EMAIL)
			return new LoginViaEmailFragment();

		return new PasswordRecoveryFragment();
	}
	
	@SuppressWarnings("unchecked")
	private Fragment getFragmentUsingReflection(final int position)
	{
		try
		{
			final Class<?> cl = getClass().getSuperclass();
			final Field field = cl.getDeclaredField("mFragments");
			field.setAccessible(true);
			
			final ArrayList<Fragment> fragments = (ArrayList<Fragment>) field.get(this);
			if (fragments.size() > position)
				return fragments.get(position);
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
}
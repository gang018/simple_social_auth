/**
 * Author - gang018
 * Email - gang1812@gmail.com
 * 03.11.2014
 */
package ru.youroute.ui.utils.socialauth;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import ru.youroute.utils.Logger;
import ru.youroute.utils.Utils;

public class FacebookAuth extends BaseSocialAuth
{
	private static final String KEY_EMAIL = "email",
								KEY_SHA = "SHA";

	private boolean isProfileRequestRunning = false;
	
	private UiLifecycleHelper mUiHelper;
	private Session.StatusCallback mCallback = new Session.StatusCallback()
	{
	    @Override
	    public void call(Session session, SessionState state, Exception exception)
	    {
	        onSessionStateChange(session, state, exception);
	    }
	};
	
	public FacebookAuth(final Activity c, final SocialLoginState st)
	{
		super(c, st);
		mUiHelper = new UiLifecycleHelper(c, mCallback);
		logKeyHash(c);
	}
	
	public FacebookAuth(final Bundle source) 
	{
		super(source);
	}
	
	private void logKeyHash(final Context c)
	{
		try {
	        PackageInfo info = c.getPackageManager().getPackageInfo(c.getApplicationContext().getPackageName(),
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance(KEY_SHA);
	            md.update(signature.toByteArray());
	            Logger.d("KeyHash: " +  Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {

	    } catch (NoSuchAlgorithmException e) {

	    }
	}
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) 
	{
	    if (state.isOpened()) 
	    {
	    	getUserData(session);
	        Logger.i("FB Logged in...");
	    } 
	    else if (state.isClosed())
	    {
	        Logger.i("FB Logged out...");
	    }
	    else if (exception != null)
	    {
	    	mSocialCallback.onLoginComplete();
	    }
	}
	
	private void getUserData(final Session s)
	{
		if (!isProfileRequestRunning)
		{
			isProfileRequestRunning = true;
			
			if (s != null)
			{
				final ProgressDialog dialog = Utils.showProgressDialog(mActivity);
				Request.newMeRequest(s, new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        Utils.closeDialog(mActivity, dialog);

                        if (user != null) {
                            final String email = user.getProperty(KEY_EMAIL).toString();
                            mState.setEmail(email);
                            mState.setState(SocialAuthState.STATE_SUCCESS);
                            mState.setProvider(getProviderName());
                            mState.setToken(user.getInnerJSONObject().toString()); //s.getAccessToken()
                            mSocialCallback.onLoginComplete();
                        }
                    }
                }).executeAsync();
			}
		}
	}

    @Override
    public void setCallback(final SocialLoginState c)
    {
        super.setCallback(c);

        if (mActivity != null)
            reinitUiHelper();
    }

    @Override
    public void setActivity(Activity act)
    {
        super.setActivity(act);
        if (mSocialCallback != null)
            reinitUiHelper();
    }

    private void reinitUiHelper()
    {
        if (mUiHelper == null)
            mUiHelper = new UiLifecycleHelper(mActivity, mCallback);
    }

    @Override
	public String getProviderName() 
	{
		return SocialAuthState.PROVIDER_FB;
	}
	
	@Override
	public void startAuthorization() 
	{
		final ArrayList<String> permissions = new ArrayList<String>();
		permissions.add(KEY_EMAIL);
		
		final Session session = Session.getActiveSession();
		if (session != null)
		{
			switch (session.getState())
			{
				case CLOSED:
				{
				}
				case CLOSED_LOGIN_FAILED:
				{
                    try
                    {
                        session.openForRead(new Session.OpenRequest(mActivity)
                                .setPermissions(permissions)
                                .setCallback(mCallback));
                    }
                    catch (Throwable e)
                    {
                        e.printStackTrace();
                        Session.openActiveSession(mActivity, true, permissions, mCallback);
                    }

					break;
				}
				
				case OPENING:
				{
				}
				case OPENED:
				{
				}
				case OPENED_TOKEN_UPDATED:
				{
				}
				case CREATED:
				{
				}
				case CREATED_TOKEN_LOADED:
				{
					Session.openActiveSession(mActivity, true, permissions, mCallback);
					break;
				}
			}
		}
		else
		{
			Session.openActiveSession(mActivity, true, permissions, mCallback);
		}
	}

	@Override
	public void onActivityResult(int arg0, int arg1, Intent arg2) 
	{
		Session.getActiveSession().onActivityResult(mActivity, arg0, arg1, arg2);
	}

	@Override
	public void onPause() 
	{
		mUiHelper.onPause();
	}

	@Override
	public void onResume() 
	{
		final Session session = Session.getActiveSession();
	    if (session != null && (session.isOpened() || session.isClosed()))
	    {
	        onSessionStateChange(session, session.getState(), null);
	    }
		
		mUiHelper.onResume();
	}

	@Override
	public void onDestroy()
	{
		mUiHelper.onDestroy();
	}
	
	@Override
	public void onCreate(final Bundle state)
	{
		mUiHelper.onCreate(state);
	}

	@Override
	public void onSaveInstanceState(Bundle state) 
	{
		mUiHelper.onSaveInstanceState(state);
	}
}
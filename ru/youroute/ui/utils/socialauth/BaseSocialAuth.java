/**
 * Author - gang018
 * Email - gang1812@gmail.com
 * 03.11.2014
 */
package ru.youroute.ui.utils.socialauth;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public abstract class BaseSocialAuth
{
	public static final String KEY_TOKEN = "token";
	public static final int REQUEST_SOCIAL_LOGIN = 890;
	
	protected SocialAuthState mState = new SocialAuthState(SocialAuthState.STATE_FAILED, null, null, null);
	protected Activity mActivity;
	protected SocialLoginState mSocialCallback;
	
	public BaseSocialAuth(final Activity act, final SocialLoginState st)
	{
		mActivity = act;
		mSocialCallback = st;
	}
	
	public BaseSocialAuth(final Bundle state)
	{
		mState = new SocialAuthState(state);
	}
	
	public SocialAuthState getState()
	{
		return mState;
	}
	
	public void setActivity(final Activity act)
	{
		if (mActivity == null)
			mActivity = act;
	}
	
	public void setCallback(final SocialLoginState c)
	{
		mSocialCallback = c;
	}
	
	public void onSaveInstanceState(final Bundle state)
	{
		if (mState != null)
			mState.saveState(state);
	}
	
	protected abstract String getProviderName();
	
	public abstract void startAuthorization();
	
	public abstract void onActivityResult(final int arg0, final int arg1, final Intent arg2);
	
	public abstract void onPause();
	
	public abstract void onResume();
	
	public abstract void onDestroy();
	
	public abstract void onCreate(final Bundle savedState);

	public interface SocialLoginState
	{
		public void onLoginComplete();
	}
}
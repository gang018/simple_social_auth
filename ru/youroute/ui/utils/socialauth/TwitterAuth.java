/**
 * Author - gang018
 * Email - gang1812@gmail.com
 * 04.11.2014
 */
package ru.youroute.ui.utils.socialauth;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.sugree.twitter.Twitter;
import com.sugree.twitter.TwitterError;
import ru.youroute.R;

public class TwitterAuth extends BaseSocialAuth
{
	private static final String CONSUMER_KEY = "",
								CONSUMER_SECRET = "";
	
	public TwitterAuth(final Activity act, final SocialLoginState st)
	{
		super(act, st);
	}

	public TwitterAuth(Bundle state)
	{
		super(state);
	}

	@Override
	protected String getProviderName()
	{
		return SocialAuthState.PROVIDER_TW;
	}

	@Override
	public void startAuthorization()
	{
		final Twitter twit = new Twitter(R.drawable.ic_launcher);
	    if(!twit.isSessionValid()) 
	    {
	        twit.authorize(mActivity, null, CONSUMER_KEY, CONSUMER_SECRET, new Twitter.DialogListener() 
	        {
				public void onTwitterError(TwitterError e)
				{
					notifyFailed();
				}
				public void onError(com.sugree.twitter.DialogError e) 
				{
					notifyFailed();
				}
				public void onCancel() 
				{
					notifyFailed();
				}
				
				public void onComplete(Bundle values) 
				{
					mActivity.runOnUiThread(new Runnable()
					{
						@Override
						public void run() 
						{
							mState.setToken(twit.getAccessToken());
							mState.setTokenSecret(twit.getSecretToken());
							mState.setProvider(getProviderName());
							mState.setState(SocialAuthState.STATE_SUCCESS);
							mSocialCallback.onLoginComplete();
						}
					});
				}
				
				private void notifyFailed()
				{
					mActivity.runOnUiThread(new Runnable()
					{
						@Override
						public void run() 
						{
							mState.setState(SocialAuthState.STATE_FAILED);
							mSocialCallback.onLoginComplete();
						}
					});
				}
			});
        }
	}

	@Override
	public void onActivityResult(int arg0, int arg1, Intent arg2)
	{
	}

	@Override
	public void onPause()
	{
	}

	@Override
	public void onResume()
	{
	}

	@Override
	public void onDestroy()
	{
	}

	@Override
	public void onCreate(Bundle savedState)
	{
	}
}
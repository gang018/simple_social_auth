/**
 * Author - gang018
 * Email - gang1812@gmail.com
 * 03.11.2014
 */
package ru.youroute.ui.utils.socialauth;
import android.os.Bundle;

public class SocialAuthState
{
	public static final String STATE_FAILED = "failed",
			STATE_SUCCESS = "success",
			PROVIDER_FB = "facebook",
			PROVIDER_VK = "vkontakte",
			PROVIDER_TW = "twitter";
	
	private static final String KEY_STATE = "state",
								KEY_EMAIL = "email",
								KEY_TOKEN = "token",
								KEY_TOKE_SECRET = "token_secret",
								KEY_PROVIDER = "provider";
	
	private String mState,
					mEmail,
					mToken,
					mProvider,
					mTokenSecret; // only for twitter
	
	public SocialAuthState(final String state, final String email, final String token, final String provider)
	{
		mState = state;
		mEmail = email;
		mProvider = provider;
		mToken = token;
	}
	
	public SocialAuthState(final Bundle state)
	{
		if (state != null)
		{
			mState = state.getString(KEY_STATE);
			mEmail = state.getString(KEY_EMAIL);
			mProvider = state.getString(KEY_PROVIDER);
			mToken = state.getString(KEY_TOKEN);
			mTokenSecret = state.getString(KEY_TOKE_SECRET);
		}
	}

	public String getState() {
		return mState;
	}

	public void setState(String mState) {
		this.mState = mState;
	}

	public String getEmail() {
		return mEmail;
	}

	public void setEmail(String mEmail) {
		this.mEmail = mEmail;
	}

	public String getToken() {
		return mToken;
	}

	public void setToken(String mToken) {
		this.mToken = mToken;
	}
	
	public String getTokenSecret() {
		return mTokenSecret;
	}
	
	public void setTokenSecret(String t) {
		this.mTokenSecret = t;
	}

	public String getProvider() {
		return mProvider;
	}

	public void setProvider(String mProvider) {
		this.mProvider = mProvider;
	}

	public void saveState(Bundle state) 
	{
		if (state != null)
		{
			state.putString(KEY_PROVIDER, mProvider);
			state.putString(KEY_STATE, mState);
			state.putString(KEY_TOKEN, mToken);
			state.putString(KEY_TOKE_SECRET, mTokenSecret);
			state.putString(KEY_EMAIL, mEmail);
		}
	}
}
/**
 * Author - gang018
 * Email - gang1812@gmail.com
 * 03.11.2014
 */
package ru.youroute.ui.utils.socialauth;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import ru.youroute.ui.activities.VkontakteLoginActivity;

public class VkontakteAuth extends BaseSocialAuth
{
	public VkontakteAuth(Activity act, BaseSocialAuth.SocialLoginState st)
	{
		super(act, st);
	}
	
	public VkontakteAuth(final Bundle source) 
	{
		super(source);
	}

	@Override
	public void startAuthorization() 
	{
		final Intent intent = new Intent(mActivity, VkontakteLoginActivity.class);
		mActivity.startActivityForResult(intent, REQUEST_SOCIAL_LOGIN);
	}

    @Override
    protected String getProviderName()
    {
        return SocialAuthState.PROVIDER_VK;
    }

    @Override
    public void onActivityResult(int arg0, int arg1, Intent arg2)
    {
        if (arg0 == REQUEST_SOCIAL_LOGIN)
        {
            if (arg1 == Activity.RESULT_OK)
            {
                final String token = arg2.getStringExtra(KEY_TOKEN);
                mState.setProvider(getProviderName());
                mState.setState(SocialAuthState.STATE_SUCCESS);
                mState.setToken(token);
                mSocialCallback.onLoginComplete();
            }
            else if (arg1 == Activity.RESULT_CANCELED)
            {
                mState.setState(SocialAuthState.STATE_FAILED);
                mSocialCallback.onLoginComplete();
            }
        }
    }

    @Override
    public void onPause()
    {
    }

    @Override
    public void onResume()
    {
    }

    @Override
    public void onDestroy()
    {
    }

    @Override
    public void onCreate(Bundle savedState)
    {
    }

    @Override
    public void onSaveInstanceState(Bundle state)
    {
    }
}